import 'package:flutter/material.dart';
import 'package:flutter_application_1/models/orang.dart';
import 'package:get/get.dart';

void main() {
  runApp(MyApp());
}

// ignore: must_be_immutable, use_key_in_widget_constructors
class MyApp extends StatelessWidget {
  var orang = Orang();

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(),
        body: Center(
            child: Obx(
          () => Text(
            "Nama saya ${orang.nama.value}",
            style: const TextStyle(
              fontSize: 35,
            ),
          ),
        )),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            orang.nama.value = orang.nama.value.toUpperCase();
          },
        ),
      ),
    );
  }
}
